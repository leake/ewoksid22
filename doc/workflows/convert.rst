Convert HDF5 to SPEC
====================

The following workflow is used for online data conversion of *stscan* HDF5 data

.. literalinclude :: ../../examples/convert.json
   :language: json

You can run it manually as follows

.. code-block:: bash

    ewoks execute examples/convert.json \
        -p wait:filename=/path/to/dataset.h5 \
        -p 'wait:entries=["1.1","2.1"]' \
        -p convert:outprefix=hc5204 \
        -p primary_outdir=/data/visitor/hc5204/id22/20230404/PROCESSED_DATA \
        --inputs=all

Workflow parameters are specified by the pattern `-p ID:NAME=VALUE` where *ID* is the id of the node
in the graph (see above) and *NAME* is the name of the parameter to be set.
