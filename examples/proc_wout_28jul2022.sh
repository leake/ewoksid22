#!/usr/bin/env bash

FILENAME=/data/id22/inhouse/id222207/id22/20220701/CCl4/CCl4_230K/CCl4_230K.h5
PROPOSAL=id222207
PARSFILE=/data/id22/inhouse/andy/CCl4_0722/CCl4_processing1/out7.pars
RESFILE=/data/id22/inhouse/andy/CCl4_0722/CCl4_processing1/temp.res
DO_CONVERT=1
DO_REBIN=1
DO_SUM=1

# Prepare results

unset HDF5_PLUGIN_PATH

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
RESULT_DIR=$SCRIPT_DIR/pyresults
mkdir -p $RESULT_DIR

# Run the convert workflow

if [[ $DO_CONVERT == 1 ]]; then
    ewoks execute $SCRIPT_DIR/convert.json \
        -p wait:filename=$FILENAME \
        -p convert:outprefix=$PROPOSAL \
        -p "convert:outdirs={\"primary\": \"$RESULT_DIR\"}"
fi

# Run the rebin workflow

if [[ $DO_REBIN == 1 ]]; then
    ewoks execute $SCRIPT_DIR/rebinsum.json \
        -p wait:filename=$FILENAME \
        -p rebin:parsfile=$PARSFILE \
        -p sum:resfile=$RESFILE \
        -p rebin:outprefix=$PROPOSAL \
        -p sum:sum_single=$DO_SUM \
        -p sum:sum_all=$DO_SUM \
        -p "rebin:outdirs={\"primary\": \"$RESULT_DIR\"}" \
        -p "convert:outdirs={\"primary\": \"$RESULT_DIR\"}" \
        -p "sum:outdirs={\"primary\": \"$RESULT_DIR\"}"
fi
