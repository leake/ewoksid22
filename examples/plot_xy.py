import os
import matplotlib.pyplot as plt
from ewoksid22.id22sum import plot_xy


filename = os.path.abspath(
    os.path.join(
        os.path.dirname(__file__),
        "..",
        "cliresults",
        "CD_GC_June2022_CeZnTi_0001_w0003.dat",
    )
)
if os.path.exists(filename):
    plot_xy(filename, 1, 2, None, True, binsize=0.002, show=False)


filename = os.path.abspath(
    os.path.join(
        os.path.dirname(__file__),
        "..",
        "pyresults",
        "CD_GC_June2022_CeZnTi_0001_w0003.dat",
    )
)
if os.path.exists(filename):
    plot_xy(filename, 1, 2, None, True, binsize=0.002, show=False)


# filename = "/data/id22/inhouse/CD_GC_PDF/advanced_50keV/patterns/for_wout/cliresults/CD_GC_June2022_CeZnTi_0001_w0003.h5"
# assert os.path.exists(filename)
# if os.path.exists(filename):
#    plot_xy(filename, 1, 2, None, True, binsize=0.002, show=False)

plt.show()
