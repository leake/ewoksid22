#!/usr/bin/env bash

rm -rf cliresults
mkdir cliresults
cd cliresults

id22rebin \
    /data/id22/inhouse/id222206/id22/CD_GC_June2022_CeZnTi/CD_GC_June2022_CeZnTi_0001/CD_GC_June2022_CeZnTi_0001.h5 \
    -o CD_GC_June2022_CeZnTi_0001_w0003.h5 \
    -p /data/id22/inhouse/CD_GC_PDF/advanced_50keV/patterns/for_wout/out7.pars \
    --device 0 \
    -r 0 inf \
    --delta2theta 0.003 \
    --startp 31

cp /data/id22/inhouse/CD_GC_PDF/advanced_50keV/patterns/for_wout/temp.res .

id22sumepy \
    -i CD_GC_June2022_CeZnTi_0001_w0003.h5 \
    -b 0.002 \
    --interactive 0
