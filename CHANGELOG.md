# CHANGELOG.md

## 0.1.0 (unreleased)

Added:
  - HDF5 to ASCII conversion
  - Wait for scan to finish
  - multianalyser data processing
  - Extract data for Topas processing